#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Template for Python file, Liam Galvin.
"""
__version__ = "2"
__author__ = "Liam Galvin"
__license__ = "GPL 3.0"

import sys
import os, os.path
import re, string
import traceback, pprint
import argparse
import time
import numpy as np
import csv

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

PGM_ROOTNAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
PGM_SRC_DIR = os.path.dirname(__file__)

class JHException(Exception):
    pass

import logging
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
LOG_LEVEL_CHOICES=["debug", "info", "warning", "error", "critical", "default"]
DEFAULT_LOG_LEVEL = "warning"
if not(DEFAULT_LOG_LEVEL in LOG_LEVEL_CHOICES):
    critical("DEFAULT_LOG_LEVEL "+str(DEFAULT_LOG_LEVEL)+ \
             " must be in LOG_LEVEL_CHOICES="+str(LOG_LEVEL_CHOICES))

def _set_log_level(log, choice=DEFAULT_LOG_LEVEL):
    c = choice.casefold()  # like lower() but for case-insensitive matching
    # log.debug('Logging level set to '+choice)
    if (c == "debug"):
        log.setLevel(logging.DEBUG)
    elif (c == "info"):
        log.setLevel(logging.INFO)
    elif (c == "warning"):
        log.setLevel(logging.WARNING)
    elif (c == "error"):
        log.setLevel(logging.ERROR)
    elif (c == "critical"):
        log.setLevel(logging.CRITICAL)
    else:
        error("Logging level {0!s} not known".format(choice))

# Establish logging
log = logging.getLogger(__name__)
_set_log_level(log)
# Log errors to the console
log_sh = logging.StreamHandler(stream=sys.stderr)
log_sh.setFormatter(logging.Formatter('%(levelname)s - Line: %(lineno)d\n  %(message)s'))
_set_log_level(log_sh,"ERROR")
log.addHandler(log_sh)
# Log most everything to a file
log_fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log')),
                         mode='w')
log_fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s'))
_set_log_level(log_fh,"INFO")
log.addHandler(log_fh)
if DEBUG:
    _set_log_level(log_sh,"DEBUG")
    _set_log_level(log_fh,"DEBUG")

def warning(s):
    t = 'WARNING: '+s+"\n"
    log.warning(t)

def error(s, level=10):
    t = 'ERROR: '+s+"\n"
    log.error(t,exc_info=True)
    sys.exit(level)

def critical(s, level=1):
    t = 'CRITICAL ERROR: '+s+"\n"
    log.critical(t,exc_info=True)
    sys.exit(level)

def mat_gen(r,c):
    """Generate a random numpy matrix with rows r and columns c
       r non-neg int number of rows in the random matrix
       c non-neg int number of columns in the random matrix
    """
    mat = np.random.random((r,c))
    with open('matrix.csv', 'w', newline='') as csvfile:
        mywriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        mywriter.writerows(mat)
def mat_in():
    #Reads in a matrix from a csv file
     with open('matrix.csv', newline='') as f:
        reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        a = []
        for row in reader:
            a.append(row)
        mat = np.array(a)
     return mat

def pivot(mat,r,a,b):
    """ Pivot a matrix row r*row1+row2
        mat 2 dimentional numpy matrix
        r   real pivot factor
        a    non-neg int index of row1
        b    non-neg int index of row2
    """
    for i in range(mat.shape[1]):
        mat[b,i] = r * mat[a,i] + mat[b,i]
    return mat

def swap(mat,a,b):
    """ Swap two rows of a matrix row 1 with row 2
        mat 2 dimentional numpy matrix
        a    non-neg int index of row1
        b    non-neg int index of row2
    """
    for i in range(mat.shape[1]):
        mat[a,i],mat[b,i] = mat[b,i],mat[a,i]
    return mat

def rescale(mat,r,a):
    """ Rescale a row a by a scale factor r
        mat 2 dimentional numpy matrix
        r   real pivot factor
        a    non-neg int index of row1
    """
    mat[a,:] = r * mat[a,:]
    return mat

def lu_factor(mat):
    r = mat.shape[0]
    c = mat.shape[1]
    indx  = np.zeros((1,c))
    for k in range(c):
        big=0
        for i in range(k,c):
            if (abs(mat[i,k]) > big):
                big = abs(mat[i,k])
                ind = i
        if (big == 0):
            print("singular matrix")
            break
        mat = swap(mat,k,ind)
        for i in range(k+1,r):
            r = -mat[i,k]/mat[k,k]
            mat = pivot(mat,r,i,k)
            print(mat)
        return mat

def mat_solve(mat):
    a = mat.shape
    for k  in range(a[0]):
        for i  in range(k+1,a[0]):
            for j  in range(a[1]):
                mat[i,j] = mat[i,j] - (mat[i,k] / mat[k,k])*mat[k,j]
    print(mat)
    return mat
    
# ===========================================================
def main(args):
    a = args.arg1
    b = args.arg2
    mat_gen(a,b)
    mat = mat_in()
    lu_factor(mat)
    

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "  Author: "+__author__
                                         +", Version: "+__version__
                                         +", License: "+__license__)
        parser.add_argument('-a', '--arg1',
                            action='store',
                            type=int,
                            default= 0,
                            help="number of rows in the  matrix. Default: {0!s}".format(0))
        parser.add_argument('-b', '--arg2',
                            action='store',
                            type=int,
                            default= 0,
                            help="number of columns  in the  matrix. Default: {0!s}".format(0))
        parser.add_argument('-D', '--debug',
                            action='store_true',
                            default=DEBUG,
                            help="Run debugging code. Default: {0!s}".format(DEBUG))
        parser.add_argument('-L', '--log_level',
                            action='store',
                            type=str,
                            choices=LOG_LEVEL_CHOICES,
                            default=DEFAULT_LOG_LEVEL,
                            help="Set the logging level. Default: {0!s}".format(DEFAULT_LOG_LEVEL))
        parser.add_argument('-v', '--version',
                            action='version',
                            version=__version__)
        parser.add_argument('-V', '--verbose',
                            action='store_true',
                            default=False,
                            help='Give verbose output. Default: {0!s}'.format(VERBOSE))
        log.info("{0!s} Started".format(parser.prog))
        args = parser.parse_args()
        _set_log_level(log,args.log_level)        
        if args.debug:
            _set_log_level(log_fh,"DEBUG")
            _set_log_level(log,"DEBUG")
        elif args.verbose:
            _set_log_level(log_fh,"INFO")
            _set_log_level(log,"INFO")
        main(args)
        _set_log_level(log,"INFO")
        log.info("{0!s} Ended.  Elapsed time {1:0.2f} sec".format(parser.prog,time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
