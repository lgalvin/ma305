#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create easier urls for latexrefman tree view
"""
__version__ = "0.0.1"
__author__ = "Jim Hefferon"
__license__ = "GPL 3.0"

import sys
import os
import traceback
import argparse
import time
import unittest

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)
import Parabola

# ==============================================
class QuadraticTestCase(unittest.TestCase):
    """Tests."""

    def test_simple(self):
        """Do the dumbest possible thing"""
        r1,r2=-2.0,-3.0
        self.assertEqual(Parabola.quadratic(1,5,6),(r1,r2))


    def testleading_zero(self):
        """Hangle the a=0 case"""
        r1,r2=1.0,1.0
        self.assertEqual(Parabola.quadratic(0,1,-1),(r1,r2),
                        "First test of a degenerate quadratic")
    def testtwo_zero(self):
        """Hangle the a=0, b=0  case"""
        r1=1
        self.assertEqual(Parabola.quadratic(0,0,1),(r1),
                        "Second test of a degenerate quadratic")

    def testsmall_dis(self):
        """Hangle the discriminat being close to b case"""
        r1,r2=-4.7912878474779195,-.20871215252208003
        self.assertEqual(Parabola.quadratic(.2,1,.2),(r1,r2),
                        "Test the close discrimiant case")
    def testsmall_square(self):
        """Hangle the small b**2"""
        r1,r2=-.6096117967977924,-1.6403882032022077
        self.assertEqual(Parabola.quadratic(2,4.5,2),(r1,r2),
                        "Test of the b square case")

# ===========================================================
def main(args):
    unittest.main()

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
