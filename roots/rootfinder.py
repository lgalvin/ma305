#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Template for Python file, Jim Hefferon.
"""
__version__ = "1"
__author__ = "Liam Galvin"
__license__ = "GPL 3.0"

import sys
import os, os.path
import re, string
import traceback, pprint
import argparse
import time

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

PGM_ROOTNAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
PGM_SRC_DIR = os.path.dirname(__file__)

class JHException(Exception):
    pass

import logging
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
LOG_LEVEL_CHOICES=["debug", "info", "warning", "error", "critical", "default"]
DEFAULT_LOG_LEVEL = "warning"
if not(DEFAULT_LOG_LEVEL in LOG_LEVEL_CHOICES):
    critical("DEFAULT_LOG_LEVEL "+str(DEFAULT_LOG_LEVEL)+ \
             " must be in LOG_LEVEL_CHOICES="+str(LOG_LEVEL_CHOICES))

def _set_log_level(log, choice=DEFAULT_LOG_LEVEL):
    c = choice.casefold()  # like lower() but for case-insensitive matching
    # log.debug('Logging level set to '+choice)
    if (c == "debug"):
        log.setLevel(logging.DEBUG)
    elif (c == "info"):
        log.setLevel(logging.INFO)
    elif (c == "warning"):
        log.setLevel(logging.WARNING)
    elif (c == "error"):
        log.setLevel(logging.ERROR)
    elif (c == "critical"):
        log.setLevel(logging.CRITICAL)
    else:
        error("Logging level {0!s} not known".format(choice))

# Establish logging
log = logging.getLogger(__name__)
_set_log_level(log)
# Log errors to the console
log_sh = logging.StreamHandler(stream=sys.stderr)
log_sh.setFormatter(logging.Formatter('%(levelname)s - Line: %(lineno)d\n  %(message)s'))
_set_log_level(log_sh,"ERROR")
log.addHandler(log_sh)
# Log most everything to a file
log_fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log')),
                         mode='w')
log_fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s'))
_set_log_level(log_fh,"INFO")
log.addHandler(log_fh)
if DEBUG:
    _set_log_level(log_sh,"DEBUG")
    _set_log_level(log_fh,"DEBUG")

def warning(s):
    t = 'WARNING: '+s+"\n"
    log.warning(t)

def error(s, level=10):
    t = 'ERROR: '+s+"\n"
    log.error(t,exc_info=True)
    sys.exit(level)

def critical(s, level=1):
    t = 'CRITICAL ERROR: '+s+"\n"
    log.critical(t,exc_info=True)
    sys.exit(level)

TOLERANCE = 0.001
DELTAX = 0.00001

def bisection(f , a, b, tol=TOLERANCE):
    """Use bisection to find a root.
    f  function of one argument
    a, b  reals  left and right endpoint
    tol  real  tolerance"""
    if (f(a)*f(b)>=0):
        return None,None
    c = a+(b-a)/2.0
    i = 0
    while (b-a)/2.0 >  tol:
        i = i+1
        if f(c) == 0:
            return c, i
        elif f(a)*f(c) < 0:
            b = c
        else:
            a = c
        c = a+(b-a)/2.0
    return c, i

def false_position(f, a, b, tol=TOLERANCE, max_iter=1000):
    """Use false position to find a root.
       f function of one argument
       a, b reals left and right endpoint
       tol real tolerance
       max_iter natural max number of iteration"""
    if (f(a)*f(b)>=0):
        return None,None
    c = a
    i = 0
    while abs(f(c)) > tol:
        i = i+1
        c = (a*f(b) - b*f(a)) / (f(b) - f(a))
        # Decide the side to repeat the steps
        if f(c) * f(a) < 0:
            b = c
        else:
            a = c
        if i >= max_iter:
            break
    return c, i

def deriv(f,pt,deltax):
    return (f(pt+deltax) - f(pt)) / deltax

def newton(f,root_est,tol=TOLERANCE,deltax=DELTAX):
    if (root_est == None):
        return None,None
    y = f(root_est)
    yprime = deriv(f,root_est,deltax)
    k = 0
    while (abs(y) > tol):
        next_root_est = root_est - (y / yprime)
        root_est = next_root_est
        y = f(root_est)
        yprime = deriv(f,root_est,deltax)
        k += 1
    return root_est,k
# ===========================================================
def main(args):
    def f(x):
        return x**2-2
    
    if args.bisection:
        c,i = bisection(f,0, 2, tol=args.tolerance)
        print("{:0.8f} (after {:d} iterations )".format(c,i))
    elif args.false_position:
        c,i = false_position(f,0, 2, tol=args.tolerance)
        print("{:0.8f} (after {:d} iterations )".format(c,i))
    else:
        c, i = newton(f, 0, tol=args.tolerance)
        print("{:0.8f} (after {:d} iterations )".format(c,i))

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "  Author: "+__author__
                                         +", Version: "+__version__
                                         +", License: "+__license__)
        parser.add_argument('-n', '--newton',
                            action='store',
                            type=bool,
                            default= False,
                            help="Use newton's method or not. Default: {0!s}".format(0))
        parser.add_argument('-b', '--bisection',
                            action='store',
                            type=bool,
                            default= False,
                            help="Use bisection  method or not. Default: {0!s}".format(0))
        parser.add_argument('-f', '--false_position',
                            action='store',
                            type=bool,
                            default= False,
                            help="Use false position  method or not. Default: {0!s}".format(0))
        parser.add_argument('-t', '--tolerance',
                            action='store',
                            type=float,
                            default= .001,
                            help="Tolerance of root finding. Default: {0!s}".format(0))
        parser.add_argument('-D', '--debug',
                            action='store_true',
                            default=DEBUG,
                            help="Run debugging code. Default: {0!s}".format(DEBUG))
        parser.add_argument('-L', '--log_level',
                            action='store',
                            type=str,
                            choices=LOG_LEVEL_CHOICES,
                            default=DEFAULT_LOG_LEVEL,
                            help="Set the logging level. Default: {0!s}".format(DEFAULT_LOG_LEVEL))
        parser.add_argument('-v', '--version',
                            action='version',
                            version=__version__)
        parser.add_argument('-V', '--verbose',
                            action='store_true',
                            default=False,
                            help='Give verbose output. Default: {0!s}'.format(VERBOSE))
        log.info("{0!s} Started".format(parser.prog))
        args = parser.parse_args()
        _set_log_level(log,args.log_level)        
        if args.debug:
            _set_log_level(log_fh,"DEBUG")
            _set_log_level(log,"DEBUG")
        elif args.verbose:
            _set_log_level(log_fh,"INFO")
            _set_log_level(log,"INFO")
        main(args)
        _set_log_level(log,"INFO")
        log.info("{0!s} Ended.  Elapsed time {1:0.2f} sec".format(parser.prog,time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
