#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test Bisection and false position program
"""
__version__ = "1"
__author__ = "Liam Galvin"
__license__ = "GPL 3.0"

import sys
import os
import traceback
import argparse
import time
import unittest
import math

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

class JHException(Exception):
    pass

def warn(s):
    t = 'WARNING: '+s+"\n"
    sys.stderr.write(t)
    sys.stderr.flush()

def error(s):
    t = 'ERROR! '+s
    sys.stderr.write(t)
    sys.stderr.flush()
    sys.exit(10)

import logging
# create file handler which logs even debug messages
log = logging.getLogger()
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
if DEBUG:
    log.setLevel(logging.DEBUG)  
else:
    log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s')
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log'
)))
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)

# ==============================================
import rootfinder

class BisectionTestCase(unittest.TestCase):
    """Bisection Tests."""
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_simple(self):
    #     """Simple root test"""
    #     def f(x):
    #         return x**2-2*x+1
    #     c,i = bisection.bisection(f,0,2, 0.001)
    #     self.assertAlmostEqual(c, 1, 3)

    def test_pos(self):
        """Postive root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,0,2, 0.001)
        self.assertAlmostEqual(c, 1, 3)

    def test_neg(self):
        """Negative root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,-2,0, 0.001)
        self.assertAlmostEqual(c, -1, 3)

    def test_irr(self):
        """Irrational root test"""
        def f(x):
            return x**2-2
        c,i = rootfinder.bisection(f,1,2, 0.0001)
        self.assertAlmostEqual(c, math.sqrt(2), 3)

    def test_sin(self):
        """Trancendental function test"""
        def f(x):
            return math.sin(x)
        c,i = rootfinder.bisection(f,3,4, 0.001)
        self.assertAlmostEqual(c,math.pi, 3)

    def test_outside(self):
        """root test with no root within braketed region"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,2,3, 0.001)
        self.assertIsNone(c)

    def test_small_root(self):
        """Very small root test"""
        def f(x):
            return x**2-.01
        c,i = rootfinder.bisection(f,0,1, 0.0001)
        self.assertAlmostEqual(c,0.1, 3)

    def test_large_root(self):
        """Large root test"""
        def f(x):
            return math.sin(x)
        k=10E6
        c,i = rootfinder.bisection(f, k, k+7, 0.0001)
        j = min(c % math.pi, -(-c % math.pi))
        self.assertAlmostEqual(j, 0, 2)
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_dual(self):
    #     """root test with braket containing both roots"""
    #     def f(x):
    #         return x**2-1
    #     c,i = bisection.bisection(f,-2,2, 0.001)
    #     self.assertAlmostEqual(c, -1, 3)

class FalsePositionTestCase(unittest.TestCase):
    """False Position Tests."""
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_simple(self):
    #     """Simple root test"""
    #     def f(x):
    #         return x**2-2*x+1
    #     c,i = bisection.bisection(f,0,2, 0.001)
    #     self.assertAlmostEqual(c, 1, 3)

    def test_pos(self):
        """Postive root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.false_position(f,0,2, 0.001)
        self.assertAlmostEqual(c, 1, 3)

    def test_neg(self):
        """Negative root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.false_position(f,-2,0, 0.001)
        self.assertAlmostEqual(c, -1, 3)

    def test_irr(self):
        """Irrational root test"""
        def f(x):
            return x**2-2
        c,i = rootfinder.false_position(f,1,2, 0.0001)
        self.assertAlmostEqual(c, math.sqrt(2), 3)

    def test_sin(self):
        """Trancendental function test"""
        def f(x):
            return math.sin(x)
        c,i = rootfinder.false_position(f,3,4, 0.001)
        self.assertAlmostEqual(c,math.pi, 3)

    def test_outside(self):
        """root test with no root within braketed region"""
        def f(x):
            return x**2-1
        c,i = rootfinder.false_position(f,2,3, 0.001)
        self.assertIsNone(c)

    def test_small_root(self):
        """Very small root test"""
        def f(x):
            return x**2-.01
        c,i = rootfinder.false_position(f,0,1, 0.0001)
        self.assertAlmostEqual(c,0.1, 3)

    def test_large_root(self):
        """Large root test"""
        def f(x):
            return math.sin(x)
        k=10E6
        c,i = rootfinder.false_position(f, k, k+7, 0.0001)
        j = min(c % math.pi, -(-c % math.pi))
        self.assertAlmostEqual(j, 0, 2)
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_dual(self):
    #     """root test with braket containing both roots"""
    #     def f(x):
    #         return x**2-1
    #     c,i = bisection.bisection(f,-2,2, 0.001)
    #     self.assertAlmostEqual(c, -1, 3)

class CompareFalsePositionBisectionTestCase(unittest.TestCase):
    """Comapirsion of Tests."""
    
    def test_irr(self):
        """Irrational root test"""
        def f(x):
            return x**2-2
        c1,i1 = rootfinder.bisection(f,1,2,0.0001)
        c2,i2 = rootfinder.false_position(f,1,2, 0.0001)
        self.assertAlmostEqual(c1, c2, 3)

    def test_roots(self):
        """Irrational root test"""
        def f3(x):
            return x**2 - 3
        def f4(x):
            return x**2 - 4
        lst=[f3,f4]
        for f in lst:
            c1,i1 = rootfinder.bisection(f,1,2,0.000001)
            c2,i2 = rootfinder.false_position(f,1,2, 0.000001)
            self.assertAlmostEqual(c1, c2, 4)

class DerivitveTestCase(unittest.TestCase):
    "Some tests of the derivitive function"
    
    def test_simple(self):
        "Simplest test of working routine"
        def f(x):
            return x**2
        c = rootfinder.deriv(f,4,0.0001)
        self.assertAlmostEqual(c,8,3)

    def test_cube(self):
        "Test a cubic function"
        def f(x):
            return x**3
        c = rootfinder.deriv(f,4,0.00001)
        self.assertAlmostEqual(c,48,3)

    def test_exp(self):
        "Test of exponetial function"
        def f(x):
            return math.exp(x)
        c = rootfinder.deriv(f,4,0.0000001)
        self.assertAlmostEqual(c,math.exp(4),3)

    def test_trig(self):
        "Test of a cosine function"
        def f(x):
            return math.cos(x)
        c = rootfinder.deriv(f,(math.pi/2),0.0001)
        self.assertAlmostEqual(c,-1,3)


TOL = 10e-9
#Tolerance of newtons method
DTOL = 10e-8
#Tolerance of derivitive function
class NewtonTestCase(unittest.TestCase):
    """Newton Tests."""
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_simple(self):
    #     """Simple root test"""
    #     def f(x):
    #         return x**2-2*x+1
    #     c,i = bisection.bisection(f,0,2, 0.001)
    #     self.assertAlmostEqual(c, 1, 3)

    def test_pos(self):
        """Postive root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,0,2, 0.001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        self.assertAlmostEqual(ans, 1, 8)

    def test_neg(self):
        """Negative root test (x-1)(x+1)"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,-2,0, 0.001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        self.assertAlmostEqual(ans, -1, 8)

    def test_irr(self):
        """Irrational root test"""
        def f(x):
            return x**2-2
        c,i = rootfinder.bisection(f,1,2, 0.0001)
        ans,itts = rootfinder.newton(f,c,TOL,DTOL)
        self.assertAlmostEqual(ans, math.sqrt(2), 8)

    def test_sin(self):
        """Trancendental function test"""
        def f(x):
            return math.sin(x)
        c,i = rootfinder.bisection(f,3,4, 0.001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        self.assertAlmostEqual(ans, math.pi, 8)

    def test_outside(self):
        """root test with no root within braketed region"""
        def f(x):
            return x**2-1
        c,i = rootfinder.bisection(f,2,3, 0.001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        self.assertIsNone(ans)

    def test_small_root(self):
        """Very small root test"""
        def f(x):
            return x**2-.01
        c,i = rootfinder.bisection(f,0,1, 0.0001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        self.assertAlmostEqual(ans,0.1, 7)

    def test_large_root(self):
        """Large root test"""
        def f(x):
            return math.sin(x)
        k=10E6
        c,i = rootfinder.bisection(f, k, k+7, 0.0001)
        ans,itt = rootfinder.newton(f,c,TOL,DTOL)
        j = min(ans % math.pi, -(-ans % math.pi))
        self.assertAlmostEqual(j, 0, 8)
    
    #Make the routine refuse to run of f(a) has the same sign as f(b)
    # def test_dual(self):
    #     """root test with braket containing both roots"""
    #     def f(x):
    #         return x**2-1
    #     c,i = bisection.bisection(f,-2,2, 0.001)
    #     self.assertAlmostEqual(c, -1, 3)
    
# ===========================================================
def main(args):
    unittest.main()

# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "\n"+__author__
                                         +" version: "+__version__
                                         +" license: "+__license__)
        parser.add_argument('-D', '--debug', action='store_true', default=DEBUG, help='run debugging code')
        parser.add_argument('-v', '--version', action='version', version=__version__)
        parser.add_argument('-V', '--verbose', action='store_true', default=False, help='verbose output')
        args = parser.parse_args()
        if args.debug:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        elif args.verbose:
            fh.setLevel(logging.INFO)
            log.setLevel(logging.INFO)
        log.info("%s Started" % parser.prog)
        main(args)
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
